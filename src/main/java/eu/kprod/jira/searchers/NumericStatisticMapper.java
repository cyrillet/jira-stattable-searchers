package eu.kprod.jira.searchers;

import static com.atlassian.jira.issue.search.util.SearchRequestAddendumBuilder.appendAndClause;
import static com.atlassian.jira.issue.search.util.SearchRequestAddendumBuilder.appendAndNotClauses;

import java.util.Comparator;

import com.atlassian.jira.issue.customfields.searchers.transformer.CustomFieldInputHelper;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.search.SearchRequestAppender;
import com.atlassian.jira.issue.search.util.SearchRequestAddendumBuilder;
import com.atlassian.jira.issue.statistics.NumericFieldStatisticsMapper;
import com.atlassian.jira.issue.statistics.util.NumericComparator;
import com.atlassian.jira.jql.builder.JqlClauseBuilder;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.dbc.Assertions;
import com.atlassian.query.operator.Operator;

/**
 * 
 * @author treym
 * 
 */
public class NumericStatisticMapper extends NumericFieldStatisticsMapper {
	private CustomField	customField;
	private CustomFieldInputHelper	customFieldInputHelper;
	private JiraAuthenticationContext	authenticationContext;

	public NumericStatisticMapper(CustomField customField, final JiraAuthenticationContext authenticationContext,
			final CustomFieldInputHelper customFieldInputHelper) {
		super(customField.getId());
		this.customField = customField;
		this.customFieldInputHelper = customFieldInputHelper;
		this.authenticationContext = authenticationContext;
	}

	public Object getValueFromLuceneField(String documentValue) {
		try {
			return new Double(documentValue);
		} catch (Exception e) {
			return null;
		}
	}

	public boolean isFieldAlwaysPartOfAnIssue() {
		return false;
	}

	public Comparator<Double> getComparator() {

		return new Comparator<Double>() {
			@Override
			public int compare(Double o1, Double o2) {

				if (o1 == null && o2 == null) {
					return 0;
				} else if (o1 == null) {
					return 1;
				} else if (o2 == null) {
					return -1;
				}
				return NumericComparator.COMPARATOR.compare(o1, o2);
			}
		};

	}

	@Override
	public int hashCode() {
		return (getDocumentConstant() != null ? getDocumentConstant().hashCode() : 0);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		final String that = ((NumericStatisticMapper) o).getDocumentConstant();

		return (getDocumentConstant() != null ? getDocumentConstant().equals(that) : that == null);
	}

	@Deprecated
	@Override
	public SearchRequest getSearchUrlSuffix(Object value, SearchRequest searchRequest) {
		return getSearchRequestAppender().appendInclusiveSingleValueClause(value, searchRequest);
	}
	
	/**
	 * @since v6.0
	 */
	public SearchRequestAppender<Object> getSearchRequestAppender() {
		return new SelectOptionSearchRequestAppender(customFieldInputHelper.getUniqueClauseName(authenticationContext.getLoggedInUser(),
				customField.getClauseNames().getPrimaryName(), customField.getName()));
	}

	static class SelectOptionSearchRequestAppender implements SearchRequestAddendumBuilder.AddendumCallback<Object>, SearchRequestAppender<Object> {
		final String	clauseName;

		public SelectOptionSearchRequestAppender(String clauseName) {
			this.clauseName = Assertions.notNull("clauseName", clauseName);	
		}

		@Override
		public void appendNonNullItem(Object value, JqlClauseBuilder clauseBuilder) {
			clauseBuilder.addNumberCondition(clauseName, Operator.EQUALS, ((Double) value).longValue());
		}

		@Override
		public void appendNullItem(JqlClauseBuilder clauseBuilder) {
			clauseBuilder.addEmptyCondition(clauseName);
		}

		@Override
		public SearchRequest appendInclusiveSingleValueClause(Object value, SearchRequest searchRequest) {
			return appendAndClause(value, searchRequest, this);
		}

		@Override
		public SearchRequest appendExclusiveMultiValueClause(Iterable<? extends Object> values, SearchRequest searchRequest) {
			return appendAndNotClauses(values, searchRequest, this);
		}
	}

}
