package eu.kprod.jira.searchers;

import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.query.ActualValueEqualityQueryFactory;
import com.atlassian.jira.jql.query.ActualValueRelationalQueryFactory;
import com.atlassian.jira.jql.query.ClauseQueryFactory;
import com.atlassian.jira.jql.query.GenericClauseQueryFactory;
import com.atlassian.jira.jql.query.LikeQueryFactory;
import com.atlassian.jira.jql.query.OperatorSpecificQueryFactory;
import com.atlassian.jira.jql.query.QueryCreationContext;
import com.atlassian.jira.jql.query.QueryFactoryResult;
import com.atlassian.jira.jql.util.IndexValueConverter;
import com.atlassian.query.clause.TerminalClause;

import java.util.ArrayList;
import java.util.List;

/**
 * Factory for producing clauses for the custom fields that have a raw index value
 *
 * @since v4.0
 */
public class ExactValueCustomFieldClauseQueryFactory implements ClauseQueryFactory
{
    private final ClauseQueryFactory delegateClauseQueryFactory;
    public ExactValueCustomFieldClauseQueryFactory(String luceneField, JqlOperandResolver operandResolver, final IndexValueConverter indexValueConverter, boolean supportsRelational)
    {
        List<OperatorSpecificQueryFactory> operatorFactories = new ArrayList<OperatorSpecificQueryFactory>();
        operatorFactories.add(new ActualValueEqualityQueryFactory(indexValueConverter));
        if (supportsRelational)
        {
            operatorFactories.add(new ActualValueRelationalQueryFactory(indexValueConverter));
        }
        operatorFactories.add(new LikeQueryFactory());
        this.delegateClauseQueryFactory = new GenericClauseQueryFactory(luceneField, operatorFactories, operandResolver);
    }

    public QueryFactoryResult getQuery(final QueryCreationContext queryCreationContext, final TerminalClause terminalClause)
    {
        return delegateClauseQueryFactory.getQuery(queryCreationContext, terminalClause);
    }
}
