package eu.kprod.jira.searchers;

/**
 * Natural order for Option.
 *
 * @author trey marc
 */
import java.util.Comparator;

import com.atlassian.jira.issue.customfields.manager.OptionsManager;
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.search.LuceneFieldSorter;

public class OptionNaturalSorter implements LuceneFieldSorter<Option> {

	private OptionsManager	optionsManager;
	private CustomField		documentConstant;

	public OptionNaturalSorter(final OptionsManager optionsManager, final CustomField customField) {
		this.optionsManager = optionsManager;
		this.documentConstant = customField;
	}

	@Override
	public Comparator<Option> getComparator() {
		return new Comparator<Option>() {
			@Override
			public int compare(Option o1, Option o2) {
				if (o1 == null && o2 == null) {
					return 0;
				} else if (o1 == null) {
					return 1;
				} else if (o2 == null) {
					return -1;
				}
				return NaturalWithNullComparator.CASE_INSENSITIVE_ORDER.compare(o1.getValue(), o2.getValue());
			}

		};
	}

	@Override
	public String getDocumentConstant() {
		return documentConstant.getId();
	}

	@Override
	public Option getValueFromLuceneField(String documentValue) {
		try {
			Long id = Long.valueOf(documentValue);
			return optionsManager.findByOptionId(id);
		} catch (NumberFormatException e) {
			return null;
		}
	}
}
