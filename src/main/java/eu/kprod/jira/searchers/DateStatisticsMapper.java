package eu.kprod.jira.searchers;

import static com.atlassian.jira.issue.search.util.SearchRequestAddendumBuilder.appendAndClause;
import static com.atlassian.jira.issue.search.util.SearchRequestAddendumBuilder.appendAndNotClauses;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;

import com.atlassian.jira.issue.customfields.searchers.transformer.CustomFieldInputHelper;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.search.SearchRequestAppender;
import com.atlassian.jira.issue.search.util.SearchRequestAddendumBuilder;
import com.atlassian.jira.issue.statistics.StatisticsMapper;
import com.atlassian.jira.jql.builder.JqlClauseBuilder;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.dbc.Assertions;
import com.atlassian.query.operator.Operator;

/**
 * Implementing StatisticsMapper for Statistics gadget.
 * 
 * @author treym
 */
public class DateStatisticsMapper implements StatisticsMapper<String>, SearchRequestAppender.Factory<String> {

	private final CustomField			customField;
	private final boolean				isDateAndTime;
	private CustomFieldInputHelper		customFieldInputHelper;
	private JiraAuthenticationContext	authenticationContext;

	/**
	 * 
	 * @param customField
	 * @param isDateAndTime
	 *            will find a better way
	 */
	public DateStatisticsMapper(CustomField customField, boolean isDateAndTime, final JiraAuthenticationContext authenticationContext,
			final CustomFieldInputHelper customFieldInputHelper) {
		this.authenticationContext = authenticationContext;
		this.customFieldInputHelper = customFieldInputHelper;
		this.customField = customField;
		this.isDateAndTime = isDateAndTime;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		final String that = ((DateStatisticsMapper) o).getDocumentConstant();
		return (getDocumentConstant() != null ? getDocumentConstant().equals(that) : that == null);
	}

	@Override
	public Comparator<String> getComparator() {
		return NaturalWithNullComparator.CASE_INSENSITIVE_ORDER;
	}

	@Override
	public String getDocumentConstant() {
		return customField.getId();
	}

	@Deprecated
	@Override
	public SearchRequest getSearchUrlSuffix(String value, SearchRequest searchRequest) {
		return getSearchRequestAppender().appendInclusiveSingleValueClause(value, searchRequest);
	}

	@Override
	public String getValueFromLuceneField(String documentValue) {
		return documentValue;
	}

	@Override
	public int hashCode() {
		return ((getDocumentConstant() != null) ? getDocumentConstant().hashCode() : 0);
	}

	@Override
	public boolean isFieldAlwaysPartOfAnIssue() {
		return false;
	}

	@Override
	public boolean isValidValue(String value) {
		return true;
	}

	/**
	 * @since v6.0
	 */
	@Override
	public SearchRequestAppender<String> getSearchRequestAppender() {
		return new SelectOptionSearchRequestAppender(customFieldInputHelper.getUniqueClauseName(authenticationContext.getLoggedInUser(),
				customField.getClauseNames().getPrimaryName(), customField.getName()), isDateAndTime);
	}

	static class SelectOptionSearchRequestAppender implements SearchRequestAddendumBuilder.AddendumCallback<String>, SearchRequestAppender<String> {
		private static final DateFormat	SDF	= new SimpleDateFormat("yyyyMMdd");
		final String					clauseName;
		final boolean					isDateAndTime;

		public SelectOptionSearchRequestAppender(String clauseName, boolean isDateAndTime) {
			this.clauseName = Assertions.notNull("clauseName", clauseName);
			this.isDateAndTime = isDateAndTime;
		}

		@Override
		public void appendNonNullItem(String value, JqlClauseBuilder clauseBuilder) {
			try {
				clauseBuilder.addDateCondition(clauseName, Operator.EQUALS, SDF.parse(isDateAndTime ? value.substring(0, 8) : value));
			} catch (ParseException e) {
				appendNullItem(clauseBuilder);
			}

		}

		@Override
		public void appendNullItem(JqlClauseBuilder clauseBuilder) {
			clauseBuilder.addEmptyCondition(clauseName);
		}

		@Override
		public SearchRequest appendInclusiveSingleValueClause(String value, SearchRequest searchRequest) {
			return appendAndClause(value, searchRequest, this);
		}

		@Override
		public SearchRequest appendExclusiveMultiValueClause(Iterable<? extends String> values, SearchRequest searchRequest) {
			return appendAndNotClauses(values, searchRequest, this);
		}
	}

}
